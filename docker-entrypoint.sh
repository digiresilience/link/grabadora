#!/bin/bash
set -e

cd ${AMIGO_DIR}

if [[ "$1" == "api" ]]; then
	echo "docker-entrypoint: starting api server"
	./cli db -- migrate
	exec dumb-init ./cli server
elif [[ "$1" == "worker" ]]; then
	echo "docker-entrypoint: starting worker"
	exec dumb-init ./cli worker
elif [[ "$1" == "frontend" ]]; then
	echo "docker-entrypoint: starting frontend"
	exec dumb-init yarn workspace @app/frontend start
elif [[ "$1" == "cli" ]]; then
	echo "docker-entrypoint: starting frontend"
	shift 1
	exec ./cli "$@"
else
	echo "docker-entrypoint: missing argument, one of: api, worker, frontend, cli"
	exit 1
fi
