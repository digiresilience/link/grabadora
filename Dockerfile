FROM node:14-buster as builder

ARG AMIGO_DIR=/opt/amigo-starter
ENV NEXT_TELEMETRY_DISABLED=1
RUN mkdir -p ${AMIGO_DIR}/
WORKDIR ${AMIGO_DIR}
COPY cli package.json yarn.lock tsconfig.json tsconfig.base.json ${AMIGO_DIR}/
COPY @app/ ${AMIGO_DIR}/@app/

# we copy the existing node modules (if any) to speed up the initial build

COPY node_modules/ ${AMIGO_DIR}/node_modules/
RUN yarn install --frozen-lockfile --production=false --no-progress
RUN npx --no-install tsc --build --verbose
RUN yarn workspace @app/frontend build

RUN rm -Rf ./node_modules ./@app/*/node_modules

RUN ls -1

FROM node:14-buster as clean
ARG AMIGO_DIR=/opt/amigo-starter

# now we copy over just what we need for production and ignore the rest

COPY --from=builder ${AMIGO_DIR}/cli ${AMIGO_DIR}/package.json ${AMIGO_DIR}/yarn.lock ${AMIGO_DIR}/

COPY --from=builder ${AMIGO_DIR}/@app/config/ ${AMIGO_DIR}/@app/config/
COPY --from=builder ${AMIGO_DIR}/@app/db/ ${AMIGO_DIR}/@app/db/

COPY --from=builder ${AMIGO_DIR}/@app/frontend/.next ${AMIGO_DIR}/@app/frontend/.next
COPY --from=builder ${AMIGO_DIR}/@app/frontend/package.json ${AMIGO_DIR}/@app/frontend/package.json

COPY --from=builder ${AMIGO_DIR}/@app/api/build ${AMIGO_DIR}/@app/api/build
COPY --from=builder ${AMIGO_DIR}/@app/api/package.json ${AMIGO_DIR}/@app/api/package.json

COPY --from=builder ${AMIGO_DIR}/@app/worker/build ${AMIGO_DIR}/@app/worker/build
COPY --from=builder ${AMIGO_DIR}/@app/worker/package.json ${AMIGO_DIR}/@app/worker/package.json

COPY --from=builder ${AMIGO_DIR}/@app/cli/build ${AMIGO_DIR}/@app/cli/build
COPY --from=builder ${AMIGO_DIR}/@app/cli/package.json ${AMIGO_DIR}/@app/cli/package.json

RUN rm -Rf ./node_modules ./@app/*/node_modules

FROM node:lts-buster
LABEL maintainer="Abel Luck <abel@guardianproject.info>"
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL="https://gitlab.com/digiresilience/link/amigo-starter"
ARG VERSION

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y --no-install-recommends \
    postgresql-client dumb-init


ARG AMIGO_DIR=/opt/amigo-starter
ENV AMIGO_DIR ${AMIGO_DIR}
RUN mkdir -p ${AMIGO_DIR}
RUN chown -R node:node ${AMIGO_DIR}/

COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

COPY --from=clean ${AMIGO_DIR}/ ${AMIGO_DIR}/

WORKDIR ${AMIGO_DIR}
RUN yarn install --frozen-lockfile --production=true --no-progress

USER node

EXPOSE 3000
EXPOSE 3001
EXPOSE 3002
ENV PORT 3000
ENV NODE_ENV production

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="digiresilience/amigo-starter"
LABEL org.label-schema.description="your friendly web app starter pack"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.vcs-url=$VCS_URL
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.version=$VERSION

ENTRYPOINT ["/docker-entrypoint.sh"]
