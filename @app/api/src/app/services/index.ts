import type * as Hapi from "@hapi/hapi";
import SettingsService from "./settings";
import RandomService from "./random";

export const register = async (server: Hapi.Server): Promise<void> => {
  // register your services here
  // don't forget to add them to the AppServices interface in ../types/index.ts
  server.registerService(RandomService);
  server.registerService(SettingsService);
};
