import type * as Hapi from "@hapi/hapi";
import NextAuthPlugin, { AdapterFactory } from "@digiresilience/hapi-nextauth";
import { NextAuthAdapter } from "@digiresilience/amigo";
import type {
  SavedUser,
  UnsavedUser,
  SavedSession,
} from "@digiresilience/amigo";
import { IAppConfig } from "../../config";

export const registerNextAuth = async (
  server: Hapi.Server,
  config: IAppConfig
): Promise<void> => {
  // I'm not sure why I need to be so explicit with the generic types here
  // I thought ts could figure out the generics based on the concrete params, but apparently not
  const nextAuthAdapterFactory: AdapterFactory<
    SavedUser,
    UnsavedUser,
    SavedSession,
    unknown
  > = (request: Hapi.Request) => new NextAuthAdapter(request.db());

  await server.register({
    plugin: NextAuthPlugin,
    options: {
      nextAuthAdapterFactory,
      sharedSecret: config.nextAuth.secret,
    },
  });
};
