import type * as Hapi from "@hapi/hapi";
import * as Joi from "joi";
import type { IAppConfig } from "../config";
import * as Services from "./services";
import * as Routes from "./routes";
import * as Plugins from "./plugins";

const AppPlugin = {
  name: "App",
  register: async (
    server: Hapi.Server,
    options: { config: IAppConfig }
  ): Promise<void> => {
    // declare our **run-time** plugin dependencies
    // these are runtime only deps, not registration time
    // ref: https://hapipal.com/best-practices/handling-plugin-dependencies
    server.dependency(["config", "hapi-pino"]);

    server.validator(Joi);
    await Plugins.register(server, options.config);
    await Services.register(server);
    await Routes.register(server);
  },
};

export default AppPlugin;
