import * as Joi from "joi";
import * as Hapi from "@hapi/hapi";
import { crudRoutesFor, CrudControllerBase } from "@digiresilience/amigo";
import * as Helpers from "../helpers";
import { VoiceLineRecord } from "@app/db";

class VoiceLineRecordController extends CrudControllerBase(VoiceLineRecord) {}

const validator = (): Record<string, Hapi.RouteOptionsValidate> => ({
  create: {
    payload: Joi.object({
      providerType: Joi.string().required(),
      providerId: Joi.string().required(),
      number: Joi.string().required(),
      language: Joi.string().required(),
      voice: Joi.string().required(),
      promptText: Joi.string().optional(),
      promptRecording: Joi.binary()
        .encoding("base64")
        .max(50 * 1000 * 1000)
        .optional(),
    }).label("VoiceLineCreate"),
  },
  updateById: {
    params: {
      id: Joi.string().uuid().required(),
    },
    payload: Joi.object({
      providerType: Joi.string().optional(),
      providerId: Joi.string().optional(),
      number: Joi.string().optional(),
      language: Joi.string().optional(),
      voice: Joi.string().optional(),
      promptText: Joi.string().optional(),
      promptRecording: Joi.binary()
        .encoding("base64")
        .max(50 * 1000 * 1000)
        .optional(),
    }).label("VoiceLineUpdate"),
  },
  deleteById: {
    params: {
      id: Joi.string().uuid().required(),
    },
  },
  getById: {
    params: {
      id: Joi.string().uuid().required(),
    },
  },
});

export const VoiceLineRoutes = async (
  _server: Hapi.Server
): Promise<Hapi.ServerRoute[]> => {
  const controller = new VoiceLineRecordController("voiceLines", "id");
  return Helpers.withDefaults(
    crudRoutesFor(
      "voice-line",
      "/api/voice-line",
      controller,
      "id",
      validator()
    )
  );
};
