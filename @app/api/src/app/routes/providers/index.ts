import * as Hapi from "@hapi/hapi";
import * as Joi from "joi";
import * as Boom from "@hapi/boom";
import Twilio from "twilio";
import * as R from "remeda";
import * as Helpers from "../helpers";

const TwilioHandlers = {
  freeNumbers: async (provider, request: Hapi.Request) => {
    const { accountSid, apiKeySid, apiKeySecret } = provider.credentials;
    const client = Twilio(apiKeySid, apiKeySecret, {
      accountSid,
    });
    const numbers = R.pipe(
      await client.incomingPhoneNumbers.list({ limit: 100 }),
      R.filter((n) => n.capabilities.voice),
      R.map(R.pick(["sid", "phoneNumber"]))
    );
    const numberSids = R.map(numbers, R.prop("sid"));
    const voiceLineRepo = request.db().voiceLines;
    const voiceLineSids = new Set(
      R.map(
        await voiceLineRepo.findAllByProviderLineSids(numberSids),
        R.prop("providerLineSid")
      )
    );

    return R.pipe(
      numbers,
      R.reject((n) => voiceLineSids.has(n.sid)),
      R.map((n) => ({ id: n.sid, name: n.phoneNumber }))
    );
  },
};

export const ProviderRoutes = Helpers.withDefaults([
  {
    method: "GET",
    path: "/api/providers/{providerId}/freeNumbers",
    options: {
      description:
        "get a list of the incoming numbers for a provider account that aren't assigned to a voice line",
      validate: {
        params: {
          providerId: Joi.string().uuid().required(),
        },
      },
      handler: async (request: Hapi.Request, _h: Hapi.ResponseToolkit) => {
        const { providerId } = request.params;
        const repo = request.db().providers;
        const provider = await repo.findById(providerId);
        if (!provider) return Boom.notFound();
        switch (provider.kind) {
          case "TWILIO":
            return TwilioHandlers.freeNumbers(provider, request);
          default:
            return Boom.badImplementation();
        }
      },
    },
  },
]);
