import config, {
  loadConfig,
  loadConfigRaw,
  IAppConfig,
  IAppConvict,
} from "@app/config";

export { IAppConvict, IAppConfig, loadConfig, loadConfigRaw };

export default config;
