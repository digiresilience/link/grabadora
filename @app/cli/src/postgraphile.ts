import { writeFileSync } from "fs";
import {
  getIntrospectionQuery,
  graphqlSync,
  lexicographicSortSchema,
  printSchema,
} from "graphql";
import { createPostGraphileSchema } from "postgraphile";
import exportPostGraphileSchema from "postgraphile/build-turbo/postgraphile/schema/exportPostGraphileSchema";
import { Pool } from "pg";
import { loadConfig } from "@app/config";
import { getPostGraphileOptions } from "@app/db";
import findWorkspaceRoot from "find-yarn-workspace-root";

export const exportGraphqlSchema = async (): Promise<void> => {
  const config = await loadConfig();

  const rootPgPool = new Pool({
    connectionString: config.db.connection,
  });
  const exportSchema = `${findWorkspaceRoot()}/data/schema.graphql`;
  const exportJson = `${findWorkspaceRoot()}/@app/frontend/src/lib/graphql-schema.json`;
  try {
    const schema = await createPostGraphileSchema(
      config.postgraphile.authConnection,
      "app_public",
      getPostGraphileOptions()
    );
    const sorted = lexicographicSortSchema(schema);
    const json = graphqlSync(schema, getIntrospectionQuery());
    writeFileSync(exportSchema, printSchema(sorted));
    writeFileSync(exportJson, JSON.stringify(json));

    console.log(`GraphQL schema exported to ${exportSchema}`);
    console.log(`GraphQL schema json exported to ${exportJson}`);
  } finally {
    rootPgPool.end();
  }
};
