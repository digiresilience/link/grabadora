require('@digiresilience/eslint-config-amigo/patch/modern-module-resolution');
module.exports = {
  extends: [
    "@digiresilience/eslint-config-amigo/profile/node",
    "@digiresilience/eslint-config-amigo/profile/typescript"
  ],
  parserOptions: { tsconfigRootDir: __dirname }
};
