import {
  RepositoryBase,
  recordInfo,
  UUID,
  Flavor,
} from "@digiresilience/amigo";

/*
 * Webhook
 *
 * A webhook allows external services to be notified when a recorded call is available
 */

export type WebhookId = Flavor<UUID, "Webhook Id">;

export interface HttpHeaders {
  header: string;
  value: string;
}

export interface UnsavedWebhook {
  name: string;
  voiceLineId: string;
  endpointUrl: string;
  httpMethod: "post" | "put";
  headers?: HttpHeaders[];
}

export interface SavedWebhook extends UnsavedWebhook {
  id: WebhookId;
  createdAt: Date;
  updatedAt: Date;
}

export const WebhookRecord = recordInfo<UnsavedWebhook, SavedWebhook>(
  "app_public",
  "webhooks"
);

export class WebhookRecordRepository extends RepositoryBase(WebhookRecord) {
  async findAllByVoiceLineId(voiceLineId: string): Promise<SavedWebhook[]> {
    return this.db.any("select * from $1 where voice_line_id = $2", [
      this.schemaTable,
      voiceLineId,
    ]);
  }
}
