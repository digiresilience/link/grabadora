import {
  RepositoryBase,
  recordInfo,
  UUID,
  Flavor,
} from "@digiresilience/amigo";

/*
 * Provider
 *
 * A provider is a company that provides incoming voice call services
 */

export type ProviderId = Flavor<UUID, "Provider Id">;

export enum ProviderKinds {
  TWILIO = "TWILIO",
}

export type TwilioCredentials = {
  accountSid: string;
  apiKeySid: string;
  apiKeySecret: string;
};

// expand this type later when we support more providers
export type ProviderCredentials = TwilioCredentials;

export interface UnsavedProvider {
  kind: ProviderKinds;
  name: string;
  credentials: ProviderCredentials;
}

export interface SavedProvider extends UnsavedProvider {
  id: ProviderId;
  createdAt: Date;
  updatedAt: Date;
}

export const ProviderRecord = recordInfo<UnsavedProvider, SavedProvider>(
  "app_public",
  "providers"
);

export class ProviderRecordRepository extends RepositoryBase(ProviderRecord) {
  async findByTwilioAccountSid(
    accountSid: string
  ): Promise<SavedProvider | null> {
    return this.db.oneOrNone(
      "select * from $1 where credentials->>'accountSid' = $2",
      [this.schemaTable, accountSid]
    );
  }
}
