import { IAppConfig } from "@app/config";
import camelcaseKeys from "camelcase-keys";
import PgSimplifyInflectorPlugin from "@graphile-contrib/pg-simplify-inflector";
// import PgManyToManyPlugin from "@graphile-contrib/pg-many-to-many";
import ConnectionFilterPlugin from "postgraphile-plugin-connection-filter";
import type { PostGraphileCoreOptions } from "postgraphile-core";

import {
  UserRecordRepository,
  AccountRecordRepository,
  SessionRecordRepository,
} from "@digiresilience/amigo";

import {
  ProviderRecordRepository,
  SettingRecordRepository,
  VoiceLineRecordRepository,
  WebhookRecordRepository,
} from "./records";

import type { IInitOptions, IDatabase } from "pg-promise";

export interface IRepositories {
  users: UserRecordRepository;
  sessions: SessionRecordRepository;
  accounts: AccountRecordRepository;
  settings: SettingRecordRepository;
  voiceLines: VoiceLineRecordRepository;
  providers: ProviderRecordRepository;
  webhooks: WebhookRecordRepository;
}

export type AppDatabase = IDatabase<IRepositories> & IRepositories;

export const dbInitOptions = (
  _config: IAppConfig
): IInitOptions<IRepositories> => {
  return {
    noWarnings: true,
    receive(data, result) {
      result.rows = camelcaseKeys(data);
    },

    // Extending the database protocol with our custom repositories;
    // API: http://vitaly-t.github.io/pg-promise/global.html#event:extend
    extend(obj: AppDatabase, _dc) {
      // Database Context (_dc) is mainly needed for extending multiple databases with different access API.

      // NOTE:
      // This event occurs for every task and transaction being executed (which could be every request!)
      // so it should be as fast as possible. Do not use 'require()' or do any other heavy lifting.
      obj.users = new UserRecordRepository(obj);
      obj.sessions = new SessionRecordRepository(obj);
      obj.accounts = new AccountRecordRepository(obj);
      obj.settings = new SettingRecordRepository(obj);
      obj.voiceLines = new VoiceLineRecordRepository(obj);
      obj.providers = new ProviderRecordRepository(obj);
      obj.webhooks = new WebhookRecordRepository(obj);
    },
  };
};

export const getPostGraphileOptions = (): PostGraphileCoreOptions => {
  return {
    ignoreRBAC: false,
    dynamicJson: true,
    ignoreIndexes: false,
    appendPlugins: [
      PgSimplifyInflectorPlugin,
      // PgManyToManyPlugin,
      ConnectionFilterPlugin,
    ],
  };
};

export * from "./helpers";
export * from "./records";
