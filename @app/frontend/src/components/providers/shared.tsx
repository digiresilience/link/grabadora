import React from "react";
import { SelectInput } from "react-admin";

export const ProviderKindInput = (props) => (
  <SelectInput
    source="kind"
    choices={[{ id: "TWILIO", name: "Twilio" }]}
    {...props}
  />
);
