import React from "react";
import {
  SimpleForm,
  TextInput,
  PasswordInput,
  Edit,
  FormDataConsumer,
  SelectInput,
  BooleanInput,
} from "react-admin";
import TwilioLanguages from "./twilio-languages";
import { ProviderKindInput, VoiceInput } from "./shared";
import MicInput from "../MicInput";

const ProviderTitle = ({ record }) => {
  let title = "";
  if (record) title = record.name ? record.name : record.email;
  return <span>Provider {title}</span>;
};

const ProviderEdit = (props) => {
  return (
    <Edit title={<ProviderTitle />} {...props}>
      <SimpleForm>
        <TextInput disabled source="id" />
        <ProviderKindInput disabled />
        <TextInput source="name" />
        <TextInput source="credentials.accountSid" />
        <TextInput source="credentials.apiKeySid" />
        <PasswordInput source="credentials.apiKeySecret" />
      </SimpleForm>
    </Edit>
  );
};

export default ProviderEdit;
