import React from "react";
import { List, Datagrid, DateField, TextField } from "react-admin";

const ProviderList = (props) => (
  <List {...props} exporter={false}>
    <Datagrid rowClick="edit">
      <TextField source="kind" />
      <TextField source="name" />
      <DateField source="createdAt" />
      <DateField source="updatedAt" />
    </Datagrid>
  </List>
);

export default ProviderList;
