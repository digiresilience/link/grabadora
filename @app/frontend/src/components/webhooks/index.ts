import WebhookIcon from "@material-ui/icons/Send";
import WebhookList from "./WebhookList";
import WebhookEdit from "./WebhookEdit";
import WebhookCreate from "./WebhookCreate";

export default {
  list: WebhookList,
  create: WebhookCreate,
  edit: WebhookEdit,
  icon: WebhookIcon,
};
