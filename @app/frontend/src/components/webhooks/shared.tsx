import React from "react";
import { SelectInput, required } from "react-admin";

const httpChoices = [
  { id: "post", name: "POST" },
  { id: "put", name: "PUT" },
];
export const HttpMethodInput = (props) => (
  <SelectInput
    source="httpMethod"
    choices={httpChoices}
    choices={httpChoices}
    validate={[required()]}
    initialValue="post"
    {...props}
  />
);
