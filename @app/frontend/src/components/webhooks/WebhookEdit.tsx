import React from "react";
import {
  SimpleForm,
  TextInput,
  Edit,
  SelectInput,
  ReferenceInput,
  ArrayInput,
  SimpleFormIterator,
  regex,
  required,
} from "react-admin";
import { HttpMethodInput } from "./shared";

const WebhookTitle = ({ record }) => {
  let title = "";
  if (record) title = record.name ? record.name : record.email;
  return <span>Webhook {title}</span>;
};

const WebhookEdit = (props) => {
  return (
    <Edit title={<WebhookTitle />} {...props}>
      <SimpleForm>
        <TextInput source="name" validate={[required()]} />
        <ReferenceInput
          label="Voice Line"
          source="voiceLineId"
          reference="voiceLines"
          validate={[required()]}
          disabled
        >
          <SelectInput optionText="number" />
        </ReferenceInput>
        <TextInput
          source="endpointUrl"
          validate={[required(), regex(/^https?:\/\/[^/]+/, "validation.url")]}
        />
        <HttpMethodInput />
        <ArrayInput source="headers">
          <SimpleFormIterator>
            <TextInput
              source="header"
              validate={[
                required(),
                regex(/^[\w-]+$/, "validation.headerName"),
              ]}
            />
            <TextInput source="value" validate={[required()]} />
          </SimpleFormIterator>
        </ArrayInput>
      </SimpleForm>
    </Edit>
  );
};

export default WebhookEdit;
