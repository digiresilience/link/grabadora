import React from "react";
import {
  SimpleForm,
  TextInput,
  Create,
  SelectInput,
  ReferenceInput,
  ArrayInput,
  SimpleFormIterator,
  regex,
  required,
} from "react-admin";
import { HttpMethodInput } from "./shared";

const WebhookCreate = (props) => {
  return (
    <Create {...props} title="Create Webhooks">
      <SimpleForm>
        <TextInput source="name" validate={[required()]} />
        <ReferenceInput
          label="Voice Line"
          source="voiceLineId"
          reference="voiceLines"
          validate={[required()]}
        >
          <SelectInput optionText="number" />
        </ReferenceInput>
        <TextInput
          source="endpointUrl"
          validate={[required(), regex(/^https?:\/\/[^/]+/, "validation.url")]}
        />
        <HttpMethodInput />
        <ArrayInput source="headers">
          <SimpleFormIterator>
            <TextInput
              source="header"
              validate={[
                required(),
                regex(/^[\w-]+$/, "validation.headerName"),
              ]}
            />
            <TextInput source="value" validate={[required()]} />
          </SimpleFormIterator>
        </ArrayInput>
      </SimpleForm>
    </Create>
  );
};

export default WebhookCreate;
