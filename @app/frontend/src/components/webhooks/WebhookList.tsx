import React from "react";
import {
  List,
  Datagrid,
  DateField,
  FunctionField,
  TextField,
  ReferenceField,
} from "react-admin";

const WebhookList = (props) => (
  <List {...props} exporter={false}>
    <Datagrid rowClick="edit">
      <TextField source="name" />
      <ReferenceField
        label="Voice Line"
        source="voiceLineId"
        reference="voiceLines"
      >
        <TextField source="number" />
      </ReferenceField>
      <DateField source="createdAt" />
      <DateField source="updatedAt" />
    </Datagrid>
  </List>
);

export default WebhookList;
