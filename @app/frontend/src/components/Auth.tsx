import { FC, useEffect } from "react";
import { CircularProgress } from "@material-ui/core";
import { useSession } from "next-auth/client";
import { useRouter } from "next/router";

export const Auth: FC = (props) => {
  const { children } = props;
  const router = useRouter();
  const [session, loading] = useSession();
  useEffect(() => {
    if (!session && !loading) {
      router.push("/login");
    }
  }, [session, loading]);

  if (loading) {
    return <CircularProgress />;
  }

  return <>{children}</>;
};
