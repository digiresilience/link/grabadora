import * as React from "react";
import { FC, useState } from "react";
import { useSelector } from "react-redux";
import SecurityIcon from "@material-ui/icons/Security";
import { useMediaQuery, Box } from "@material-ui/core";
import { useTranslate, MenuItemLink, MenuProps } from "react-admin";
import users from "../users";
import accounts from "../accounts";
import voicelines from "../voicelines";
import providers from "../providers";
import webhooks from "../webhooks";

import SubMenu from "./SubMenu";

type MenuName = "menuCatalog" | "menuUsers" | "menuCustomers";

const Menu: FC<MenuProps> = ({ onMenuClick, logout, dense = false }) => {
  const [state, setState] = useState({
    menuUsers: true,
  });
  const translate = useTranslate();
  const isXSmall = useMediaQuery((theme) => theme.breakpoints.down("xs"));
  const open = useSelector((state) => state.admin.ui.sidebarOpen);
  useSelector((state) => state.theme); // force rerender on theme change

  const handleToggle = (menu: MenuName) => {
    setState((state) => ({ ...state, [menu]: !state[menu] }));
  };

  //<DashboardMenuItem onClick={onMenuClick} sidebarIsOpen={open} />
  return (
    <Box mt={1}>
      {" "}
      <MenuItemLink
        to={`/providers`}
        primaryText={translate(`resources.providers.name`, {
          smart_count: 2,
        })}
        leftIcon={<providers.icon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />
      <MenuItemLink
        to={`/voicelines`}
        primaryText={translate(`resources.voicelines.name`, {
          smart_count: 2,
        })}
        leftIcon={<voicelines.icon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />
      <MenuItemLink
        to={`/webhooks`}
        primaryText={translate(`resources.webhooks.name`, {
          smart_count: 2,
        })}
        leftIcon={<webhooks.icon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />
      <SubMenu
        handleToggle={() => handleToggle("menuUsers")}
        isOpen={state.menuUsers}
        sidebarIsOpen={open}
        name="pos.menu.security"
        icon={<SecurityIcon />}
        dense={dense}
      >
        <MenuItemLink
          to={`/users`}
          primaryText={translate(`resources.users.name`, {
            smart_count: 2,
          })}
          leftIcon={<users.icon />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
        <MenuItemLink
          to={`/accounts`}
          primaryText={translate(`resources.accounts.name`, {
            smart_count: 2,
          })}
          leftIcon={<accounts.icon />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
      </SubMenu>
      {isXSmall && logout}
    </Box>
  );
};

export default Menu;
