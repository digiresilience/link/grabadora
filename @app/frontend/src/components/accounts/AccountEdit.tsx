import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  SimpleForm,
  TextInput,
  Edit,
  ReferenceInput,
  SelectInput,
  DateInput,
  Toolbar,
  DeleteButton,
} from "react-admin";
import { useSession } from "next-auth/client";

const useStyles = makeStyles((_theme) => ({
  defaultToolbar: {
    flex: 1,
    display: "flex",
    justifyContent: "space-between",
  },
}));

const AccountEditToolbar = (props) => {
  const [session] = useSession();
  const classes = useStyles(props);
  return (
    <Toolbar className={classes.defaultToolbar} {...props}>
      <DeleteButton disabled={session.user.id === props.record.userId} />
    </Toolbar>
  );
};

const AccountTitle = ({ record }) => {
  let title = "";
  if (record) title = record.name ? record.name : record.email;
  return <span>Account {title}</span>;
};

export const AccountEdit = (props) => (
  <Edit title={<AccountTitle />} {...props}>
    <SimpleForm toolbar={<AccountEditToolbar />}>
      <TextInput disabled source="id" />
      <ReferenceInput source="userId" reference="users">
        <SelectInput disabled optionText="email" />
      </ReferenceInput>
      <TextInput disabled source="providerType" />
      <TextInput disabled source="providerId" />
      <TextInput disabled source="providerAccountId" />
      <DateInput disabled source="createdAt" />
      <DateInput disabled source="updatedAt" />
    </SimpleForm>
  </Edit>
);
export default AccountEdit;
