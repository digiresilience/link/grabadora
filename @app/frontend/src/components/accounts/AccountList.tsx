import React from "react";
import {
  List,
  Datagrid,
  DateField,
  TextField,
  ReferenceField,
  DeleteButton,
} from "react-admin";
import { useSession } from "next-auth/client";

const DeleteNotSelfButton = (props) => {
  const [session] = useSession();
  return (
    <DeleteButton
      disabled={session.user.id === props.record.userId}
      {...props}
    />
  );
};

export const AccountList = (props) => (
  <List {...props} exporter={false}>
    <Datagrid rowClick="edit">
      <ReferenceField source="userId" reference="users">
        <TextField source="email" />
      </ReferenceField>
      <TextField source="providerType" />
      <TextField source="providerId" />
      <TextField source="providerAccountId" />
      <DateField source="createdAt" />
      <DateField source="updatedAt" />
      <DeleteNotSelfButton />
    </Datagrid>
  </List>
);

export default AccountList;
