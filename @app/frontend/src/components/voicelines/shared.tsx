import React, { useState, useEffect } from "react";
import PlayIcon from "@material-ui/icons/PlayCircleFilled";
import {
  TextInput,
  SelectInput,
  required,
  useTranslate,
  useNotify,
} from "react-admin";
import { IconButton, CircularProgress } from "@material-ui/core";
import absoluteUrl from "../../lib/absolute-url";
import TwilioLanguages from "./twilio-languages";

const tts = async (providerId) => {
  const r = await fetch(`/api/v1/twilio/text-to-speech-token/${providerId}`);
  const { token } = await r.json();
  const twilioClient = await import("twilio-client");
  return (voice, language, prompt) =>
    new Promise((resolve) => {
      if (!voice || !language || !prompt) resolve();
      const Device = twilioClient.Device;
      const device = new Device();
      const silence = `${absoluteUrl().origin}/static/silence.mp3`;
      device.setup(token, {
        codecPrefences: ["opus", "pcmu"],
        enableRingingState: false,
        fakeLocalDTMF: true,
        disableAudioContextSounds: true,
        sounds: {
          disconnect: silence,
          incoming: silence,
          outgoing: silence,
        },
      });
      device.on("ready", function (device) {
        device.connect({ language, voice, prompt });
      });
      device.on("disconnect", () => resolve());
      device.on("error", () => resolve());
    });
};

export const TextToSpeechButton = ({ form }) => {
  const { providerId, language, voice, promptText: prompt } = form.formData;
  const [loading, setLoading] = useState(false);
  const [ttsProvider, setTTSProvider] = useState(false);
  const [playText, setPlayText] = useState(false);
  useEffect(async () => {
    if (providerId) {
      setLoading(true);
      setTTSProvider({ provider: await tts(providerId) });
      setLoading(false);
    }
  }, [form.formData.providerId]);

  useEffect(async () => {
    setPlayText({
      func: async () => {
        setLoading(true);
        await ttsProvider.provider(voice, language, prompt);
        setLoading(false);
      },
    });
  }, [prompt, language, voice, ttsProvider.provider]);

  const disabled = !(providerId && prompt?.length >= 2 && voice && language);
  return (
    <IconButton
      onClick={playText.func}
      disabled={disabled}
      variant="contained"
      color="primary"
    >
      {!loading && <PlayIcon />}
      {loading && <CircularProgress size={20} />}
    </IconButton>
  );
};
export const PromptInput = (form, ...rest) => {
  return (
    <TextInput
      source="promptText"
      multiline
      options={{ fullWidth: true }}
      InputProps={{ endAdornment: <TextToSpeechButton form={form} /> }}
      {...rest}
    />
  );
};

const validateVoice = (args, values) => {
  if (!values.language) return "validation.language";
  if (!values.voice) return "validation.voice";

  const availableVoices = TwilioLanguages.voices[values.language];
  const found =
    availableVoices.filter((v) => v.id === values.voice).length === 1;
  if (!found) return "validation.voice";
};

export const VoiceInput = (form, ...rest) => {
  const voice = TwilioLanguages.voices[form.formData.language] || [];
  return (
    <SelectInput
      source="voice"
      choices={voice}
      validate={[required(), validateVoice]}
      {...rest}
    />
  );
};

let noAvailableNumbers = false;
let availableNumbers = [];

const getAvailableNumbers = async (providerId) => {
  try {
    const r = await fetch(`/api/v1/providers/${providerId}/freeNumbers`);
    availableNumbers = await r.json();
    noAvailableNumbers = availableNumbers.length === 0;
    return availableNumbers;
  } catch (error) {
    console.error(
      `Could not fetch available numbers for provider ${providerId}`
    );
    return [];
  }
};

const sidToNumber = (sid) => {
  return availableNumbers
    .filter(({ id, name }) => id === sid)
    .map(({ name }) => name)[0];
};

export const populateNumber = (data) => {
  return {
    ...data,
    number: sidToNumber(data.providerLineSid),
  };
};

const hasNumbers = (args, value, values, translate, ...props) => {
  if (noAvailableNumbers) return "validation.noAvailableNumbers";
};

export const AvailableNumbersInput = (form, ...rest) => {
  const {
    meta: { touched, error } = {},
    input: { ...inputProps },
    ...props
  } = rest;
  const translate = useTranslate();
  const notify = useNotify();
  const [loading, setLoading] = useState(false);
  const [choices, setChoices] = useState(false);
  useEffect(async () => {
    if (form && form.formData && form.formData.providerId) {
      setLoading(true);
      const choices = await getAvailableNumbers(form.formData.providerId);
      setChoices({
        choices,
        helperText: noAvailableNumbers
          ? translate("validation.noAvailableNumbers")
          : "",
      });
      if (noAvailableNumbers) notify("validation.noAvailableNumbers", "error");
      setLoading(false);
    }
  }, [form && form.formData ? form.formData.providerId : undefined]);

  return (
    <>
      <SelectInput
        label="Number"
        source="providerLineSid"
        choices={choices.choices}
        disabled={loading}
        validate={[hasNumbers, required()]}
        error={!!(touched && error) || !!choices.helperText}
        helperText={choices.helperText}
        {...inputProps}
        {...props}
      />
      {loading && <CircularProgress />}
    </>
  );
};
