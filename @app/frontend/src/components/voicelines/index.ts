import VoiceLineIcon from "@material-ui/icons/PhoneCallback";
import VoiceLineList from "./VoiceLineList";
import VoiceLineEdit from "./VoiceLineEdit";
import VoiceLineCreate from "./VoiceLineCreate";

export default {
  list: VoiceLineList,
  create: VoiceLineCreate,
  edit: VoiceLineEdit,
  icon: VoiceLineIcon,
};
