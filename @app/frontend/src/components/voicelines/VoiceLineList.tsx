import React from "react";
import {
  List,
  Datagrid,
  DateField,
  FunctionField,
  TextField,
  ReferenceField,
} from "react-admin";

const VoiceLineList = (props) => (
  <List {...props} exporter={false}>
    <Datagrid rowClick="edit">
      <ReferenceField
        label="Provider"
        source="providerId"
        reference="providers"
      >
        <FunctionField render={(p) => `${p.kind}: ${p.name}`} />
      </ReferenceField>
      <TextField source="number" />
      <TextField source="language" />
      <TextField source="voice" />
      <DateField source="createdAt" />
      <DateField source="updatedAt" />
    </Datagrid>
  </List>
);

export default VoiceLineList;
