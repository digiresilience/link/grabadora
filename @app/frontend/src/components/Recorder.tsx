import React, { FC, useState } from "react";
import dynamic from "next/dynamic";

const ReactMic = dynamic(
  () => import("react-mic").then((mod) => mod.ReactMic),
  { ssr: false }
);

export const Mic: FC = () => {
  const isBrowser = typeof window !== undefined;
  let [record, setRecorder] = useState({ record: false });
  let [blob, setBlob] = useState("");
  const startRecording = () => {
    console.log("START");
    setRecorder({ record: true });
  };

  const stopRecording = () => {
    console.log("STOP");
    setRecorder({ record: false });
  };

  async function onData(recordedBlob) {
    console.log("chunk of real-time data is: ", recordedBlob);

    //trying to convert the blob to a wav file
  }

  function onStop(recordedBlob) {
    console.log("recordedBlob is: ", recordedBlob);
    setBlob(recordedBlob.blobURL);
  }
  return (
    <div>
      <ReactMic
        record={record.record}
        className="sound-wave"
        onStop={onStop}
        onData={onData}
        strokeColor="#000000"
        backgroundColor="grey"
        mimeType="audio/webm"
      />
      <button onClick={startRecording} type="button">
        Start
      </button>
      <button onClick={stopRecording} type="button">
        Stop
      </button>
      <div id="audiocontrols">
        <audio controls src={blob}></audio>
      </div>
    </div>
  );
};

export default Mic;
