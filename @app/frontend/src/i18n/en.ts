import { TranslationMessages } from "react-admin";
import englishMessages from "ra-language-english";

const customEnglishMessages: TranslationMessages = {
  ...englishMessages,

  auth: {
    loggingIn: "Logging in...",
  },
  pos: {
    configuration: "Configuration",
    menu: {
      security: "Security",
      accounts: "Accounts",
      voicelines: "Voice Lines",
      providers: "Provider",
      webhooks: "Webhooks",
    },
  },
  resources: {
    users: {
      name: "User |||| Users",
    },
    accounts: {
      name: "Account |||| Accounts",
    },
    voicelines: {
      name: "Voice Line |||| Voice Lines",
      fields: {
        providerLineSid: "Provider Line SID",
      },
    },
    providers: {
      name: "Provider |||| Providers",
      fields: {
        credentials: {
          accountSid: "Twilio Account SID",
          apiKeySid: "Twilio API Key SID",
          apiKeySecret: "Twilio API Key Secret",
        },
      },
    },
    webhooks: {
      name: "Webhook |||| Webhooks",
      fields: {
        endpointUrl: "Endpoint URL",
        httpMethod: "HTTP Method",
        headers: "HTTP Headers",
        header: "Header Name",
        value: "Header Value",
      },
    },
  },
  validation: {
    url: "a valid url starting with https:// is required",
    voice: "a voice is required",
    language: "a language is required",
    headerName: "a valid http header name has only letters, numbers and dashes",
    noAvailableNumbers:
      "There are no available numbers to assign. Please visit the provider and purchase more numbers.",
  },
};

export default customEnglishMessages;
