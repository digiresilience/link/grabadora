import {
  ApolloClient,
  InMemoryCache,
  ApolloLink,
  HttpLink,
} from "@apollo/client";
import { onError } from "@apollo/client/link/error";

const errorLink = onError(
  ({ operation, graphQLErrors, networkError, forward }) => {
    console.log("ERROR LINK", operation);
    if (graphQLErrors)
      graphQLErrors.map(({ message, locations, path, ...rest }) =>
        console.log(
          `[GraphQL error]: Message: ${message}`,
          locations,
          path,
          rest
        )
      );
    if (networkError) console.log(`[Network error]: ${networkError}`);
    console.log(Object.keys(networkError));
    forward(operation);
  }
);

export const apolloClient = new ApolloClient({
  link: ApolloLink.from([errorLink, new HttpLink({ uri: "/graphql" })]),
  cache: new InMemoryCache(),
  /*
  defaultOptions: {
    watchQuery: {
      fetchPolicy: "no-cache",
      errorPolicy: "ignore",
    },
    query: {
      fetchPolicy: "no-cache",
      errorPolicy: "all",
    },
  },*/
});
