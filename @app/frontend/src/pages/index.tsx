import { Typography, Box, Button, Grid, Link } from "@material-ui/core";
import { FC, useEffect } from "react";
import { useRouter } from "next/router";

export const RedirectToAdmin: FC = (props) => {
  const { children } = props;
  const router = useRouter();
  useEffect(() => {
    router.push("/admin");
  });

  return <>{children}</>;
};

export default function Home() {
  return (
    <Box>
      <Typography variant="h3">AmigoStarter</Typography>
      <Grid container justify="space-around" style={{ padding: 60 }}>
        <Grid item>
          <Link href="/admin">
            <Button variant="contained">Admin</Button>
            <RedirectToAdmin />
          </Link>
        </Grid>
      </Grid>
    </Box>
  );
}
