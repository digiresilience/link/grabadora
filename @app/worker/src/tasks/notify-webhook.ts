import Wreck from "@hapi/wreck";
import * as R from "remeda";
import { withDb, AppDatabase } from "../db";

export interface WebhookPayload {
  startTime: string;
  endTime: string;
  to: string;
  from: string;
  duration: string;
  callSid: string;
  recording: string;
  mimeType: string;
}
export interface WebhookOptions {
  webhookId: string;
  payload: WebhookPayload;
}

const notifyWebhooksTask = async (options: WebhookOptions): Promise<void> =>
  withDb(async (db: AppDatabase) => {
    const { webhookId, payload } = options;

    const webhook = await db.webhooks.findById({ id: webhookId });
    if (!webhook) return;

    const { endpointUrl, httpMethod, headers } = webhook;
    const headersFormatted = R.reduce(
      headers || [],
      (acc, h) => {
        acc[h.header] = h.value;
        return acc;
      },
      {}
    );

    const wreck = Wreck.defaults({
      json: true,
      headers: headersFormatted,
    });

    // http errors will bubble up and cause the job to fail and be retried
    try {
      await (httpMethod === "post"
        ? wreck.post(endpointUrl, { payload })
        : wreck.put(endpointUrl, { payload }));
    } catch (error) {
      console.log(error.output);
      throw new Error(
        `webhook failed webhookId=${webhookId} callSid=${payload.callSid}`
      );
    }
  });

export default notifyWebhooksTask;
