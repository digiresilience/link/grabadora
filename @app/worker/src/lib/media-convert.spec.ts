import { readFileSync, writeFileSync, unlinkSync } from "fs";
import process from "process";
import ffmpeg from "fluent-ffmpeg";
import { convert, selfCheck } from "./media-convert";

describe("media converter", () => {
  it("self tests", async () => {
    expect(await selfCheck()).toBeTruthy();
  });
  it("converts webm to mp3", async () => {
    expect.assertions(4);
    console.log("CWD", process.cwd());
    const webm = readFileSync("./fixtures/test.webm");

    const mp3 = await convert(webm);
    expect(mp3).not.toBeNull();

    writeFileSync("./fixtures/out.mp3", mp3);

    return new Promise((resolve) => {
      ffmpeg("./fixtures/out.mp3").ffprobe(0, (err, data) => {
        if (err) console.log("ERR", err);
        console.log("DATA", data);
        expect(err).toBeNull();
        expect(data.streams).toHaveLength(1);
        expect(data.format.format_name).toBe("mp3");
        unlinkSync("./fixtures/out.mp3");
        resolve(data);
      });
    });
  });
});
