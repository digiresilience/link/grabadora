import csv
import sys
import json
import re
from itertools import groupby
from itertools import groupby


def key_func(k):
    return k["group"]


pat1 = r"(.*) \(([^)]*)\)[^(]*$"
pat2 = r"(.*?) \(.*$"
f = open(sys.argv[1], encoding="utf-8-sig")
reader = csv.DictReader(f)
data = []
languages = []
voices = {}
for row in reader:
    capture = re.search(pat2, row["Locale"])
    group = capture.group(1)
    capture = re.search(pat1, row["Locale"])
    name = capture.group(1)
    id = capture.group(2)
    languages.append({"id": id, "name": name})
    if id not in voices:
        voices[id] = []
    voices[id].append({"id": "Polly." + row["Voice"], "name": row["Voice"]})


print(json.dumps({"languages": languages, "voices": voices}))
